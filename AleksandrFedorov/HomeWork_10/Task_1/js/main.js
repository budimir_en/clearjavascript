var messenger = (function() {
    var count = 0;
    var firstName; 
    var secondName;

    function setFirstName(fName) {
        firstName = "Mr. " + fName;
    }

    function setSecondName(sName) {
        secondName = sName;
    }
    
    function getMessageText() {
        var greetingMessage = "Welcome " + firstName + " ";
        greetingMessage += secondName + ". Glad to see you ";
        return greetingMessage;
    }

    return {
        init: function(fName, sName) {
            setFirstName(fName);
            setSecondName(sName);
        },
        sayHello: function() {
            console.log(getMessageText() + "Count " + (count++));
        },
        resetCount: function() {
            count = 0;
            console.log("Count is 0");
        }
    }
})();
messenger.init("John", "Smith");
messenger.sayHello();
